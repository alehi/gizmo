package com.mobilegeraete.kassel.uni.gizmo;

import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Hunger;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Play;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Thirst;

import org.junit.Test;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.junit.Assert.assertEquals;

/**
 * Created by alehi on 27.12.2016.
 */

public class DataModelTest {

    @Test
    public void propertyChangeAttr() throws Exception {

        Dlw dlw = new Dlw();

        dlw.withName("Juergen");

        class PclTest implements PropertyChangeListener {
            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {

                Dlw dlw = (Dlw) propertyChangeEvent.getSource();

                dlw.setName("propertyChangeD");
            }
        }

        PclTest listener = new PclTest();

        //add listener
        dlw.addPropertyChangeListener(Dlw.PROPERTY_NAME, listener);

        //manipulate attribute
        dlw.setName("android");

        //check PropertyChangeEvent
        assertEquals("propertyChangeD", dlw.getName());

        //remove pcl
        dlw.removePropertyChangeListener(Dlw.PROPERTY_NAME, listener);

        //manipulate attribute
        dlw.setName("android");

        //check name
        assertEquals("android", dlw.getName());

        //manipulate attribute
        dlw.setName(null);

        //check name
        assertEquals(null, dlw.getName());

    }

    @Test
    public void propertyChangeAssoc() throws Exception {

        Dlw dlw = new Dlw();

        dlw.withName("Juergen");

        class PclTest implements PropertyChangeListener {
            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {

                Dlw dlw = (Dlw) propertyChangeEvent.getSource();

                dlw.setName("propertyChangeD");
            }
        }

        PclTest listener = new PclTest();

        //add listener
        dlw.addPropertyChangeListener(Dlw.PROPERTY_NEEDS, listener);

        //manipulate assoc
        dlw.withNeeds(new Thirst());

        //check PropertyChangeEvent
        assertEquals("propertyChangeD", dlw.getName());
        dlw.withName("Juergen");

        //manipulate assoc
        dlw.withNeeds(new Play());

        //check PropertyChangeEvent
        assertEquals("propertyChangeD", dlw.getName());
        dlw.withName("Juergen");
        //manipulate assoc
        dlw.withoutNeeds(dlw.getNeeds().get(0));

        //check PropertyChangeEvent
        assertEquals("propertyChangeD", dlw.getName());
        dlw.withName("Juergen");


        //remove pcl
        dlw.removePropertyChangeListener(Dlw.PROPERTY_NEEDS, listener);

        //manipulate assoc
        dlw.withNeeds(new Thirst());

        //check PropertyChangeEvent
        assertEquals("Juergen", dlw.getName());

    }

    @Test
    public void arraylist() throws Exception {

        Dlw dlw = new Dlw();

        dlw.withName("Juergen");

        dlw.withNeeds(new Hunger());
        Thirst thirst = new Thirst();
        dlw.withNeeds(thirst);
        //remove hunger
        dlw.withoutNeeds(dlw.getNeeds().get(0));

        assertEquals(thirst, dlw.getNeeds().get(0));

        //check ref. integ.
        assertEquals(dlw, thirst.getDlw());
        dlw.withoutNeeds(thirst);
        assertEquals(null, thirst.getDlw());
    }

}
