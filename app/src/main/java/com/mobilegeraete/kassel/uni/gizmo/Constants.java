package com.mobilegeraete.kassel.uni.gizmo;

/**
 * Created by Fury on 05.01.2017.
 */

public class Constants {

    // Store
    public static int MAX_SELECTIONS = 5;
    public static final long SHOPPING_DURATION = 900000;
    //public static final long SHOPPING_DURATION = 60000;
    // 900.000 mSec = 15 Min

    //ITEMS
    public static int MAX_MEALS_SUPPLIES = 5;
    public static int MAX_DRINKS_SUPPLIES = 5;
    public static int MAX_STUFF_SUPPLIES = 5;

    //Time to satisfy Needs (in milliseconds)
    public static final int SATISFY_PLAY = 1000 * 60 * 90;
    public static final int SATISFY_DRESS = 1000 * 60 * 45;
    public static final int SATISFY_HUNGER = 1000 * 60 * 45;
    public static final int SATISFY_THIRST = 1000 * 60 * 45;
    public static final long SATISFY_WALK = 1000 * 60 * 90;


    //Needs timing
    public static final int DRESS_HOUR_1 = 8; //daySuite time
    public static final int DRESS_HOUR_2 = 20;  //nightSuite time

    public static final int MIN_SECONDES_TILL_NEXT_PLAY = 60 * 60 * 6;
    public static final int MAX_SECONDES_TILL_NEXT_PLAY = 60 * 60 * 8;

    public static final int MIN_MINUTES_TILL_NEXT_WALK = 24 * 60;
    public static final int MAX_MINUTES_TILL_NEXT_WALK = 24 * 60;

    //has to be in ascending order
    public static final int HUNGER_HOUR_1 = 8; //breakfast time
    public static final int HUNGER_HOUR_2 = 13; //lunch time
    public static final int HUNGER_HOUR_3 = 18; //dinner time

    //has to be in ascending order
    public static final int THIRST_HOUR_1 = 8;
    public static final int THIRST_HOUR_2 = 11;
    public static final int THIRST_HOUR_3 = 13;
    public static final int THIRST_HOUR_4 = 15;
    public static final int THIRST_HOUR_5 = 18;

    //Shakeing threshold and Limit
    public static final int SHAKE_THRESHOLD = 700;
    public static final int DONE_WITH_THE_DISHES = 200;

    //WashingLimit
    public static final int DONE_WITH_WASHING = 10;

    //Step limits
    public static final int STEP_LIMIT = 15;

    //Number of sayings
    public static final int NO_OF_SAYINGS = 10;
}
