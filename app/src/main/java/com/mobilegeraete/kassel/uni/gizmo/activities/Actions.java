package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dress;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Hunger;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Needs;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Play;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Thirst;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Walk;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;


public class Actions extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actions);

        //Set up actionBar for Actions activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.action);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.actions));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);

        //checks forwarding extra, added by the BackgroundService into the Notification intent
        String to = getIntent().getStringExtra("forwardTo");
        Log.d("forwarding", "actionActivity");
        if ("play".equals(to)) {
            Intent intent = new Intent(this, LetsPlay.class);
            startActivity(intent);
        } else if ("takingAWalk".equals(to)) {
            Intent intent = new Intent(this, TakingAWalk.class);
            startActivity(intent);
        }

    }

    //This method is being called by clicking on the Laundry icon
    //The user can access the DoingLaundry activity if he has dirty laundry (itemCount of cloth1 and cloth2 > 0) and washingAgent (itemCount > 0).
    public void goToTheLaundryRoom(View v) {
        //Get the dlw object, that contains the current state of the game
        Dlw dlw = Persistence.getDlw();
        //Prevent NullPointerException, if dlw = null
        if (dlw != null) {
            //Get the itemCount of the washingAgent and the dirtyCloth.
            int washingAgentCount = dlw.getStorage().findItem(getString(R.string.startActivityClean1)).getCount();
            int laundryCloth1 = dlw.getStorage().findItem(getString(R.string.startActivityCloth1)).getCount();
            int laundryCloth2 = dlw.getStorage().findItem(getString(R.string.startActivityCloth2)).getCount();

            //if there is dirtyLaundry and enough washingAgent the intent is created and the DoingLaundry activity is started.
            if (laundryCloth1 != Constants.MAX_STUFF_SUPPLIES || laundryCloth2 != Constants.MAX_STUFF_SUPPLIES) {
                if (washingAgentCount > 0) {
                    Intent intent = new Intent(this, DoingLaundry.class);
                    startActivity(intent);
                } else {
                    //washingAgent itemCount is below 1, which means that the user can't clean the laundry.
                    Toast.makeText(this, getString(R.string.noWashingAgentToast), Toast.LENGTH_LONG).show();
                }
            } else {
                //The laundry is already clean. The user doesn't need do access the laundryActivity
                Toast.makeText(this, getString(R.string.noDirtyLaundryToast), Toast.LENGTH_LONG).show();
            }
        }
    }

    //This method is being called by clicking on the dishes icon
    //The user can access the DoingDishes activity if he has dirty dishes (itemCount of cups and plates is 0) and dishSoap (itemCount > 0).
    public void letsDoTheDishes(View v) {
        //Get the dlw object, that contains the current state of the game
        Dlw dlw = Persistence.getDlw();
        //Prevent NullPointerException, if dlw = null
        if (dlw != null) {

            //Get the itemCount of the dishSoap and the cloth1 and cloth2.
            int dishSoapCount = dlw.getStorage().findItem(getString(R.string.startActivityClean2)).getCount();
            int dirtyCupCount = dlw.getStorage().findItem(getString(R.string.startActivityCup)).getCount();
            int dirtyPlateCount = dlw.getStorage().findItem(getString(R.string.startActivityPlate)).getCount();

            //if there are dirty dishes (cup and plate itemCount = 0) and enough dishSoap (itemCount > 0) the intent is created and the DoingDishes activity is started.
            if (dirtyCupCount != Constants.MAX_STUFF_SUPPLIES || dirtyPlateCount != Constants.MAX_STUFF_SUPPLIES) {
                if (dishSoapCount > 0) {
                    Intent intent = new Intent(this, DoingDishes.class);
                    startActivity(intent);
                } else {
                    //dishSoap itemCount is below 1, which means that the user can't clean the dishes.
                    Toast.makeText(this, getString(R.string.noDishSoapToast), Toast.LENGTH_LONG).show();
                }
            } else {
                //The dishes are already clean (itemCount of the cup and plate items is equal to Constants.MAX_STUFF_SUPPLIES). The user doesn't need do access the DoingDishes activity
                Toast.makeText(this, getString(R.string.noDirtyDishesToast), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void goToLetsPlayActivity(View v) {
        Dlw dlw = Persistence.getDlw();
        boolean playNeedFound = false;
        if (dlw != null) {
            for (Needs need : dlw.getNeeds()) {
                if (need instanceof Play) {
                    playNeedFound = true;
                    break;
                }
            }

            if (playNeedFound) {
                Intent intent = new Intent(this, LetsPlay.class);
                startActivity(intent);
            } else {
                Toast.makeText(Actions.this, dlw.getName() + " " + getString(R.string.actionsPlayMissingT), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void goToTakingAWalkActivity(View v) {
        Dlw dlw = Persistence.getDlw();
        boolean walkNeedFound = false;

        if (dlw != null) {
            for (Needs need : dlw.getNeeds()) {
                if (need instanceof Walk) {
                    walkNeedFound = true;
                    break;
                }
            }

            if (walkNeedFound) {
                Intent intent = new Intent(this, TakingAWalk.class);
                startActivity(intent);
            } else {
                Toast.makeText(Actions.this, dlw.getName() + " " + getString(R.string.actionsWalkMissingT), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void feedTheDlw(View v) {
        Dlw dlw = Persistence.getDlw();
        Hunger currentHunger = null;
        if (dlw != null) {
            for (Needs need : dlw.getNeeds()) {
                if (need instanceof Hunger) {
                    currentHunger = (Hunger) need;
                    break;
                }
            }

            // if something is missing, display an error toasts and return
            if (currentHunger == null) {
                Toast.makeText(Actions.this, dlw.getName() + " " + getString(R.string.actionsHungerMissingT), Toast.LENGTH_SHORT).show();
                return;
            }
            if (dlw.getStorage().findItem(getString(R.string.startActivityPlate)).getCount() <= 0) {
                Toast.makeText(Actions.this, String.format(getString(R.string.actionsPlateMissingT), getString(R.string.startActivityPlate)), Toast.LENGTH_SHORT).show();
                return;
            }
            if (dlw.getStorage().findItem(currentHunger.getRequiredItem()).getCount() <= 0) {
                Toast.makeText(Actions.this, String.format(getString(R.string.actionsMealsOrDrinksMissingT), currentHunger.getRequiredItem()), Toast.LENGTH_SHORT).show();
                return;
            }

            //everything is available..
            //.. satisfy need
            int dishesCount = dlw.getStorage().findItem(getString(R.string.startActivityPlate)).getCount();
            dlw.getStorage().findItem(getString(R.string.startActivityPlate)).setCount(dishesCount - 1);

            int itemCount = dlw.getStorage().findItem(currentHunger.getRequiredItem()).getCount();
            dlw.getStorage().findItem(currentHunger.getRequiredItem()).setCount(itemCount - 1);

            dlw.withoutNeeds(currentHunger);

            Toast.makeText(Actions.this, getString(R.string.actionsSuccessT), Toast.LENGTH_SHORT).show();
        }
    }

    public void serveDrinks(View v) {
        Dlw dlw = Persistence.getDlw();
        Thirst currentThirst = null;
        if (dlw != null) {
            for (Needs need : dlw.getNeeds()) {
                if (need instanceof Thirst) {
                    currentThirst = (Thirst) need;
                    break;
                }
            }

            // if something is missing, display an error toasts and return
            if (currentThirst == null) {
                Toast.makeText(Actions.this, dlw.getName() + " " + getString(R.string.actionsThirstMissingT), Toast.LENGTH_SHORT).show();
                return;
            }
            if (dlw.getStorage().findItem(getString(R.string.startActivityCup)).getCount() <= 0) {
                Toast.makeText(Actions.this, String.format(getString(R.string.actionsCupMissingT), getString(R.string.startActivityCup)), Toast.LENGTH_SHORT).show();
                return;
            }
            if (dlw.getStorage().findItem(currentThirst.getRequiredItem()).getCount() <= 0) {
                Toast.makeText(Actions.this, String.format(getString(R.string.actionsMealsOrDrinksMissingT), currentThirst.getRequiredItem()), Toast.LENGTH_SHORT).show();
                return;
            }

            //everything is available..
            //.. satisfy need
            int dishesCount = dlw.getStorage().findItem(getString(R.string.startActivityCup)).getCount();
            dlw.getStorage().findItem(getString(R.string.startActivityCup)).setCount(dishesCount - 1);

            int itemCount = dlw.getStorage().findItem(currentThirst.getRequiredItem()).getCount();
            dlw.getStorage().findItem(currentThirst.getRequiredItem()).setCount(itemCount - 1);

            dlw.withoutNeeds(currentThirst);

            Toast.makeText(Actions.this, getString(R.string.actionsSuccessT), Toast.LENGTH_SHORT).show();
        }
    }

    public void changeClothes(View v) {
        Dlw dlw = Persistence.getDlw();
        Dress currentDress = null;
        if (dlw != null) {

            for (Needs need : dlw.getNeeds()) {
                if (need instanceof Dress) {
                    currentDress = (Dress) need;
                    break;
                }
            }

            // if something is missing, display an error toasts and return
            if (currentDress == null) {
                Toast.makeText(Actions.this, dlw.getName() + " " + getString(R.string.actionsDressMissingT), Toast.LENGTH_SHORT).show();
                return;
            }
            if (dlw.getStorage().findItem(currentDress.getRequiredItem()).getCount() <= 0) {
                Toast.makeText(Actions.this, String.format(getString(R.string.actionsClothesMissingT), currentDress.getRequiredItem()), Toast.LENGTH_SHORT).show();
                return;
            }

            //everything is available..
            //.. satisfy need
            int itemCount = dlw.getStorage().findItem(currentDress.getRequiredItem()).getCount();
            dlw.getStorage().findItem(currentDress.getRequiredItem()).setCount(itemCount - 1);


            dlw.withoutNeeds(currentDress);

            Toast.makeText(Actions.this, getString(R.string.actionsSuccessT), Toast.LENGTH_SHORT).show();
        }
    }
}
