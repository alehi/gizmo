package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Needs;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Play;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

public class LetsPlay extends AppCompatActivity {

    private Play currentPlay = null;

    private String[] sayingsStart;
    private String[] sayingsGap;
    private String[] sayingsEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lets_play);

        //Set up actionBar for letsPLay activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.play);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.play));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);


        Dlw dlw = Persistence.getDlw();

        Resources res = getResources();
        sayingsStart = res.getStringArray(R.array.letsPlaySayinsStart);
        sayingsGap = res.getStringArray(R.array.letsPlaySayinsGap);
        sayingsEnd = res.getStringArray(R.array.letsPlaySayinsEnd);

        //find play object
        for (Needs need : dlw.getNeeds()) {
            if (need instanceof Play) {
                currentPlay = (Play) need;
                break;
            }
        }


        TextView sayingText = (TextView) findViewById(R.id.letsPlaySayingTV);

        if (currentPlay != null) {
            int sayingNo = Integer.valueOf(currentPlay.getRequiredItem());
            sayingText.setText(sayingsStart[sayingNo]
                    + " __________ " + sayingsEnd[sayingNo]);
        } else {
            sayingText.setText(this.getString(R.string.letsPlayErrorMsg));
        }
    }

    public void submitAnswer(View v) {

        if (currentPlay == null) return;

        EditText editTextUserAnswer = (EditText) findViewById(R.id.letsPlayAnswerET);
        String userAnswer = editTextUserAnswer.getText().toString();
        userAnswer = userAnswer.toLowerCase();
        userAnswer = userAnswer.trim();

        String answer = sayingsGap[Integer.valueOf(currentPlay.getRequiredItem())];
        answer = answer.toLowerCase();
        answer = answer.trim();


        if (answer.equals(userAnswer)) {
            Persistence.getDlw().withoutNeeds(currentPlay);

            Toast.makeText(LetsPlay.this, getString(R.string.letsPlayRightAnswer), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(this, MainMenu.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
            Variables.backToMainMenu = true;
        } else {
            Toast.makeText(LetsPlay.this, getString(R.string.letsPlayWrongAnswer), Toast.LENGTH_SHORT).show();
            editTextUserAnswer.setText("");
        }
    }
}
