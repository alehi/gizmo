package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Cup;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Item;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Plate;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;


public class DoingDishes extends AppCompatActivity {

    View myView;

    private TextView doingDishesCleanerTV, doingDishesDoneTV, doingDishesPlateTV, doingDishesCupTV;

    //Progressbar
    private ProgressBar progressbar;

    //Set to false, when the user isn't done with the dishes. Switches to true when he is done
    boolean dishesDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doing_dishes);

        //Set up actionBar for doingDishes activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.wash);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.washingDishes));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);

        //Get the current dlw
        Dlw dlw = Persistence.getDlw();

        //Get the view
        myView = findViewById(R.id.activity_doing_dishes);
        //Register touchListener
        myView.setOnTouchListener(handleTouch);

        //Get the gui elements
        doingDishesDoneTV = (TextView) findViewById(R.id.doingDishesDoneTV);
        doingDishesCleanerTV = (TextView) findViewById(R.id.doingDishesCleanerTV);
        doingDishesPlateTV = (TextView) findViewById(R.id.doingDishesPlateTV);
        doingDishesCupTV = (TextView) findViewById(R.id.doingDishesCupTV);


        // TextView info for user before cleaning
        doingDishesCleanerTV.setText(this.getString(R.string.doingDishesCleaner) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityClean2)).getCount());
        doingDishesPlateTV.setText(this.getString(R.string.doingDishesCleanPlates) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityPlate)).getCount());
        doingDishesCupTV.setText(this.getString(R.string.doingDishesCleanCups) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityCup)).getCount());

        progressbar = (ProgressBar) findViewById(R.id.doingDishesProgressbar);
        progressbar.setMax(Constants.DONE_WITH_THE_DISHES);
        Drawable progressBarDrawable0 = getResources().getDrawable(R.drawable.progress_dishwashing0);
        progressbar.setProgressDrawable(progressBarDrawable0);
        dishesDone = false;
    }

    private View.OnTouchListener handleTouch = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            switch (event.getAction()) {

                //case MotionEvent.ACTION_MOVE: When the user moves with his/her finger in the display
                case MotionEvent.ACTION_MOVE: {
                    //increase wipeCount
                    Variables.wipeCount++;

                    //Set new progress to progressbar
                    progressbar.setProgress(Variables.wipeCount);

                    break;
                }
                //case MotionEvent.ACTION_UP: When the user removes his/her finger from the display,
                // the wipeCountVariable is set to 0. The user hat so start from the beginning
                case MotionEvent.ACTION_UP: {
                    //Optional
                    Variables.wipeCount = 0;

                    progressbar.setProgress(Variables.wipeCount);
                    break;
                }
            }

            // Progress Drawables
            Drawable progressBarDrawable1 = getResources().getDrawable(R.drawable.progress_dishwashing1);
            Drawable progressBarDrawable2 = getResources().getDrawable(R.drawable.progress_dishwashing2);
            Drawable progressBarDrawable3 = getResources().getDrawable(R.drawable.progress_dishwashing3);
            Drawable progressBarDrawable4 = getResources().getDrawable(R.drawable.progress_dishwashing4);
            Drawable progressBarDrawable5 = getResources().getDrawable(R.drawable.progress_dishwashing5);

            if (progressbar.getProgress() >= (Constants.DONE_WITH_THE_DISHES * 0.16)) {
                progressbar.setProgressDrawable(progressBarDrawable1);
            }
            if (progressbar.getProgress() > (Constants.DONE_WITH_THE_DISHES * 0.32)) {
                progressbar.setProgressDrawable(progressBarDrawable2);
            }
            if (progressbar.getProgress() > (Constants.DONE_WITH_THE_DISHES * 0.64)) {
                progressbar.setProgressDrawable(progressBarDrawable3);
            }
            if (progressbar.getProgress() > (Constants.DONE_WITH_THE_DISHES * 0.80)) {
                progressbar.setProgressDrawable(progressBarDrawable4);
            }

            //When the user wipes on the display of the device a specific number of times (Constants.DONE_WITH_THE_DISHES)
            //the Number of available cups and plates is set to the value of MAX_STUFF_SUPPLIES
            if (Variables.wipeCount >= Constants.DONE_WITH_THE_DISHES) {
                Dlw dlw = Persistence.getDlw();

                //Toast.makeText(this, "Fertig mit Waschen!", Toast.LENGTH_SHORT).show();

                //Set itemCount of cup and plate item to the maximum
                for (Item item : dlw.getStorage().getItems()) {
                    if (item instanceof Cup || item instanceof Plate) {
                        item.withCount(Constants.MAX_STUFF_SUPPLIES);
                    }
                }

                //Subtract dishSoap
                if (!dishesDone) {
                    int dishSoapCount = dlw.getStorage().findItem(getString(R.string.startActivityClean2)).getCount();
                    dishSoapCount--;
                    dlw.getStorage().findItem(getString(R.string.startActivityClean2)).withCount(dishSoapCount);
                    dishesDone = true;
                }
                // Progressbar after cleaning
                progressbar.setProgressDrawable(progressBarDrawable5);
                // TextView info for user after cleaning
                doingDishesCleanerTV.setText(getString(R.string.doingDishesCleaner) + ": " + dlw.getStorage().findItem(getString(R.string.startActivityClean2)).getCount());
                doingDishesPlateTV.setText(getString(R.string.doingDishesCleanPlates) + ": " + dlw.getStorage().findItem(getString(R.string.startActivityPlate)).getCount());
                doingDishesCupTV.setText(getString(R.string.doingDishesCleanCups) + ": " + dlw.getStorage().findItem(getString(R.string.startActivityCup)).getCount());
                doingDishesDoneTV.setText("" + getString(R.string.doingDishesDone) + ".");
            }
            return true;
        }
    };

    @Override
    protected void onStop() {
        super.onStop();

        //Toast.makeText(this, "onPause", Toast.LENGTH_LONG).show();

        //Set variable to 0 when the user switches to a new activity
        Variables.wipeCount = 0;
    }

    //Go to main menu
    public void goToMainMenu(View v) {
        Intent intent = new Intent(this, MainMenu.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;
    }
}
