package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dress;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Hunger;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Play;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Thirst;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Walk;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

import java.util.ArrayList;

public class TestingNeeds extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing_actions);

        //Set up actionBar for TestingNeeds activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.dummy);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.testingActionsTitleTV));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);
    }


    public void HungerNow(View v) {

        ArrayList<String> meals = new ArrayList<>();
        meals.add(this.getString(R.string.startActivityMeal1));
        meals.add(this.getString(R.string.startActivityMeal2));
        meals.add(this.getString(R.string.startActivityMeal3));
        meals.add(this.getString(R.string.startActivityMeal4));
        meals.add(this.getString(R.string.startActivityMeal5));
        meals.add(this.getString(R.string.startActivityMeal6));
        meals.add(this.getString(R.string.startActivityMeal7));
        meals.add(this.getString(R.string.startActivityMeal8));
        meals.add(this.getString(R.string.startActivityMeal9));

        Hunger hunger = new Hunger();
        hunger.setGeneratedByTest(true);
        hunger.withRequiredItems(meals.get((int) (System.currentTimeMillis() % 9)));
        hunger.withStartTime(System.currentTimeMillis());
        Persistence.getDlw().withNeeds(hunger);
    }

    public void PlayNow(View v) {
        Play play = new Play();
        play.setGeneratedByTest(true);
        play.withStartTime(System.currentTimeMillis());
        play.setRequiredItem("" + System.currentTimeMillis() % Constants.NO_OF_SAYINGS);
        Persistence.getDlw().withNeeds(play);
    }

    public void ThirstNow(View v) {

        ArrayList<String> drinks = new ArrayList<>();
        drinks.add(this.getString(R.string.startActivityDrink1));
        drinks.add(this.getString(R.string.startActivityDrink2));
        drinks.add(this.getString(R.string.startActivityDrink3));

        Thirst thirst = new Thirst();
        thirst.setGeneratedByTest(true);
        thirst.withRequiredItems(drinks.get((int) (System.currentTimeMillis() % 3)));
        thirst.withStartTime(System.currentTimeMillis() + 10000);
        Persistence.getDlw().withNeeds(thirst);
    }

    public void DressNow(View v) {
        Dress dress = new Dress();
        dress.setGeneratedByTest(true);
        dress.withRequiredItems(System.currentTimeMillis() % 2 == 0 ? this.getString(R.string.startActivityCloth1) : this.getString(R.string.startActivityCloth2));
        dress.withStartTime(System.currentTimeMillis());
        Persistence.getDlw().withNeeds(dress);
    }

    public void WalkNow(View v) {
        Walk walk = new Walk();
        walk.setGeneratedByTest(true);
        walk.withStartTime(System.currentTimeMillis());
        Persistence.getDlw().withNeeds(walk);
    }

    public void HungerSoon(View v) {
        ArrayList<String> meals = new ArrayList<>();
        meals.add(this.getString(R.string.startActivityMeal1));
        meals.add(this.getString(R.string.startActivityMeal2));
        meals.add(this.getString(R.string.startActivityMeal3));
        meals.add(this.getString(R.string.startActivityMeal4));
        meals.add(this.getString(R.string.startActivityMeal5));
        meals.add(this.getString(R.string.startActivityMeal6));
        meals.add(this.getString(R.string.startActivityMeal7));
        meals.add(this.getString(R.string.startActivityMeal8));
        meals.add(this.getString(R.string.startActivityMeal9));

        Hunger hunger = new Hunger();
        hunger.setGeneratedByTest(true);
        hunger.withRequiredItems(meals.get((int) (System.currentTimeMillis() % 9)));
        hunger.withStartTime(System.currentTimeMillis() + 10000);
        Persistence.getDlw().withNextNeeds(hunger);
    }

    public void PlaySoon(View v) {
        Play play = new Play();
        play.setGeneratedByTest(true);
        play.withStartTime(System.currentTimeMillis() + 10000);
        play.setRequiredItem("" + System.currentTimeMillis() % Constants.NO_OF_SAYINGS);
        Persistence.getDlw().withNextNeeds(play);
    }

    public void ThirstSoon(View v) {
        Thirst thirst = new Thirst();
        thirst.setGeneratedByTest(true);
        ArrayList<String> drinks = new ArrayList<>();
        drinks.add(this.getString(R.string.startActivityDrink1));
        drinks.add(this.getString(R.string.startActivityDrink2));
        drinks.add(this.getString(R.string.startActivityDrink3));
        thirst.withRequiredItems(drinks.get((int) (System.currentTimeMillis() % 3)));
        thirst.withStartTime(System.currentTimeMillis() + 10000);
        Persistence.getDlw().withNextNeeds(thirst);
    }

    public void DressSoon(View v) {
        Dress dress = new Dress();
        dress.setGeneratedByTest(true);
        dress.withRequiredItems(System.currentTimeMillis() % 2 == 0 ? this.getString(R.string.startActivityCloth1) : this.getString(R.string.startActivityCloth2));
        dress.withStartTime(System.currentTimeMillis() + 10000);
        Persistence.getDlw().withNextNeeds(dress);
    }

    public void WalkSoon(View v) {
        Walk walk = new Walk();
        walk.setGeneratedByTest(true);
        walk.withStartTime(System.currentTimeMillis() + 10000);
        Persistence.getDlw().withNextNeeds(walk);
    }

    public void goToMainMenu(View v) {
        Intent intent = new Intent(this, MainMenu.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;
    }
}
