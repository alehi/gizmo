/*
   Copyright (c) 2016 alehi
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mobilegeraete.kassel.uni.gizmo.datamodel;

import android.content.Context;

import com.google.gson.annotations.Expose;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Calendar;

public class Needs {


    //==========================================================================

    /********************************************************************
     * <pre>
     *              many                       one
     * Needs ----------------------------------- Dlw
     *              needs                   dlw
     * </pre>
     */

    public static final String PROPERTY_DLW = "dlw";
    public static final String PROPERTY_DLW2 = "dlw2";
    public static final String PROPERTY_STARTTIME = "startTime";
    public static final String PROPERTY_REQUIREDITEM = "requiredItem";
    public static final String PROPERTY_GENERATEDBYTEST = "generatedByTest";

    protected PropertyChangeSupport listeners = null;
    private Dlw dlw = null;
    private Dlw dlw2 = null;
    @Expose
    private long startTime;
    @Expose
    private String requiredItem;
    @Expose
    private boolean generatedByTest;


    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }


    //==========================================================================

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    public void removeYou() {
        setDlw(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    public long getStartTime() {
        return this.startTime;
    }

    public void setStartTime(long value) {

        if (this.startTime != value) {

            long oldValue = this.startTime;
            this.startTime = value;
            this.firePropertyChange(PROPERTY_STARTTIME, oldValue, value);
        }
    }

    public Needs withStartTime(long value) {
        setStartTime(value);
        return this;
    }

    public boolean getGeneratedByTest() {
        return this.generatedByTest;
    }

    public void setGeneratedByTest(boolean value) {

        if (this.generatedByTest != value) {

            boolean oldValue = this.generatedByTest;
            this.generatedByTest = value;
            this.firePropertyChange(PROPERTY_GENERATEDBYTEST, oldValue, value);
        }
    }

    public Needs withGeneratedByTest(boolean value) {
        setGeneratedByTest(value);
        return this;
    }



    public Dlw getDlw() {
        return this.dlw;
    }

    public boolean setDlw(Dlw value) {
        boolean changed = false;

        if (this.dlw != value) {
            Dlw oldValue = this.dlw;

            if (this.dlw != null) {
                this.dlw = null;
                oldValue.withoutNeeds(this);
            }

            this.dlw = value;

            if (value != null) {
                value.withNeeds(this);
            }

            firePropertyChange(PROPERTY_DLW, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Needs withDlw(Dlw value) {
        setDlw(value);
        return this;
    }

    public Dlw getDlw2() {
        return this.dlw2;
    }

    public boolean setDlw2(Dlw value) {
        boolean changed = false;

        if (this.dlw2 != value) {
            Dlw oldValue = this.dlw2;

            if (this.dlw2 != null) {
                this.dlw2 = null;
                oldValue.withoutNextNeeds(this);
            }

            this.dlw2 = value;

            if (value != null) {
                value.withNextNeeds(this);
            }

            firePropertyChange(PROPERTY_DLW2, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Needs withDlw2(Dlw value) {
        setDlw2(value);
        return this;
    }

    public Dlw createDlw() {
        Dlw value = new Dlw();
        withDlw(value);
        return value;
    }

    public String getRequiredItem() {
        return this.requiredItem;
    }

    public void setRequiredItem(String value) {

        if (value == null || !value.equals(this.requiredItem)) {

            String oldValue = this.requiredItem;
            this.requiredItem = value;
            this.firePropertyChange(PROPERTY_REQUIREDITEM, oldValue, value);
        }
    }

    public Needs withRequiredItems(String value) {
        setRequiredItem(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(startTime);

        result.append(" ").append("startTime: "+ calendar.getTime().toString());
        result.append(" ").append("required item: "+requiredItem);
        return result.substring(1);
    }

    public String getNeedInfo(Context context){
        return "";
    }

    public long getDeadline(){
        return 0;
    }
}
