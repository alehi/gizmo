package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Clothing;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Item;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

//Sensor imports

public class DoingLaundry extends AppCompatActivity implements SensorEventListener {

    TextView doingLaundryCleanerTV, doingLaundryDoneTV, doingLaundryDaySuitTV, doingLaundryNightSuitTV;

    //Progressbar
    private ProgressBar progressbar;
    private Drawable progressBarDrawable5;

    //Sensor
    private Sensor shakeCountSensor;
    private SensorManager sM;

    //Variables to store the last values
    long lastUpdate;
    float lastX, lastY, lastZ;

    //Variable to check, when the laundry is done
    boolean laundryDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doing_laundry);

        Dlw dlw = Persistence.getDlw();

        //Set up actionBar for doingLaundry activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.laundry);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.doingLaundry));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);

        //Initialize values
        lastUpdate = 0;
        lastX = 0;
        lastY = 0;
        lastZ = 0;

        //Creating sensor managers
        sM = (SensorManager) getSystemService(SENSOR_SERVICE);

        //Accelerometer sensor
        shakeCountSensor = sM.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        progressbar = (ProgressBar) findViewById(R.id.doingLaundryProgressbar);
        progressbar.setMax(Constants.DONE_WITH_WASHING);

        //Get gui elements
        doingLaundryDoneTV = (TextView) findViewById(R.id.doingLaundryDoneTV);
        doingLaundryCleanerTV = (TextView) findViewById(R.id.doingLaundryCleanerTV);
        doingLaundryDaySuitTV = (TextView) findViewById(R.id.doingLaundryDaySuitTV);
        doingLaundryNightSuitTV = (TextView) findViewById(R.id.doingLaundryNightSuitTV);

        // TextView info for user before cleaning
        doingLaundryCleanerTV.setText(this.getString(R.string.doingLaundryCleaner) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityClean1)).getCount());
        doingLaundryDaySuitTV.setText(this.getString(R.string.doingLaundryCleanDaySuits) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityCloth1)).getCount());
        doingLaundryNightSuitTV.setText(this.getString(R.string.doingLaundryCleanNightSuits) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityCloth2)).getCount());

        Drawable progressBarDrawable0 = getResources().getDrawable(R.drawable.progress_laundrycleaning0);
        progressbar.setProgressDrawable(progressBarDrawable0);

        //Set to false, when the user reaches the shake limit the variable is set to true.
        laundryDone = false;
    }


    @Override
    protected void onPause() {
        super.onPause();

        //Reset shakeCount when the user leaves the activity
        //shakeCount = 0;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        //tVX.setText("X: " + sensorEvent.values[0]);
        //tVY.setText("Y: " + sensorEvent.values[1]);
        //tVZ.setText("Z: " + sensorEvent.values[2]);

        Drawable progressBarDrawable1 = getResources().getDrawable(R.drawable.progress_laundrycleaning1);
        Drawable progressBarDrawable2 = getResources().getDrawable(R.drawable.progress_laundrycleaning2);
        Drawable progressBarDrawable3 = getResources().getDrawable(R.drawable.progress_laundrycleaning3);
        Drawable progressBarDrawable4 = getResources().getDrawable(R.drawable.progress_laundrycleaning4);

        //Get the current time. The current time is needed to calculate the speed and to prevent more than one update each 100ms
        long currentTime = System.currentTimeMillis();
        // Update not more than every 100ms
        if ((currentTime - lastUpdate) > 100) {
            //Calculate the time difference from now and the last sensorEvent
            long diffTime = (currentTime - lastUpdate);

            //Set the new "lastUpdate"  time to the current time
            lastUpdate = currentTime;

            //Get the current values from the sensorEvent to calculate the speed
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            //Add all three new sensor values and substract the values of the last event. Divide that by diffTime (see above) and multiply it bay 10000 to get [m/s].
            float speed = Math.abs(x + y + z - lastX - lastY - lastZ) / diffTime * 10000;
            //If speed is greater than the SHAKE_THRESHOLD it counts as one shake
            if (speed > Constants.SHAKE_THRESHOLD) {
                //Log.d("sensor", "shake detected w/ speed: " + speed);
                //Tracks the number of shakes
                Variables.shakeCount++;
                //Toast.makeText(this, "shake detected w/ speed: " + speed, Toast.LENGTH_SHORT).show();
            }
            lastX = x;
            lastY = y;
            lastZ = z;

            //Set new progress
            progressbar.setProgress(Variables.shakeCount);

            // Progress Drawables
            if (progressbar.getProgress() >= (Constants.DONE_WITH_WASHING * 0.16)) {
                progressbar.setProgressDrawable(progressBarDrawable1);
            }
            if (progressbar.getProgress() > (Constants.DONE_WITH_WASHING * 0.32)) {
                progressbar.setProgressDrawable(progressBarDrawable2);
            }
            if (progressbar.getProgress() > (Constants.DONE_WITH_WASHING * 0.64)) {
                progressbar.setProgressDrawable(progressBarDrawable3);
            }
            if (progressbar.getProgress() > (Constants.DONE_WITH_WASHING * 0.80)) {
                progressbar.setProgressDrawable(progressBarDrawable4);
            }

            //When the user shakes the device a specific number of times (Constants.DONE_WITH_WASHING) the Number of available "Cloth" is set to the value of MAX_STUFF_SUPPLIES
            if (Variables.shakeCount >= Constants.DONE_WITH_WASHING) {
                Dlw dlw = Persistence.getDlw();

                //Toast.makeText(this, "Fertig mit Waschen!", Toast.LENGTH_SHORT).show();
                //Iterate thru all items in storage.
                for (Item item : dlw.getStorage().getItems()) {
                    //check if an object is of typ clothing
                    if (item instanceof Clothing) {
                        //Set itemCount to Constants.MAX_STUFF_SUPPLIES
                        item.withCount(Constants.MAX_STUFF_SUPPLIES);
                    }
                }

                if (!laundryDone) {
                    //Get the washingAgentCount and reduce it by 1
                    int washingAgentCount = dlw.getStorage().findItem(getString(R.string.startActivityClean1)).getCount();
                    washingAgentCount--;
                    dlw.getStorage().findItem(getString(R.string.startActivityClean1)).withCount(washingAgentCount);
                    //The laundry is done. Set variable to true
                    laundryDone = true;
                }
                // TextView info for user after cleaning
                doingLaundryCleanerTV.setText(this.getString(R.string.doingLaundryCleaner) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityClean1)).getCount());
                doingLaundryDaySuitTV.setText(getString(R.string.doingLaundryCleanDaySuits) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityCloth1)).getCount());
                doingLaundryNightSuitTV.setText(getString(R.string.doingLaundryCleanNightSuits) + ": " + dlw.getStorage().findItem(this.getString(R.string.startActivityCloth2)).getCount());
                doingLaundryDoneTV.setText("" + getString(R.string.doingDishesDone) + ".");
                // Progressbar after cleaning
                progressBarDrawable5 = getResources().getDrawable(R.drawable.progress_laundrycleaning5);
                progressbar.setProgressDrawable(progressBarDrawable5);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        //Register sensor listener
        sM.registerListener(this, shakeCountSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onStop() {
        super.onStop();

        //Reset shakeCount. The user has to start from the beginning after leaving the activity
        Variables.shakeCount = 0;
        sM.unregisterListener(this, shakeCountSensor);
    }

    //Go to mainMenu
    public void goToMainMenu(View v) {
        Intent intent = new Intent(this, MainMenu.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;
    }
}
