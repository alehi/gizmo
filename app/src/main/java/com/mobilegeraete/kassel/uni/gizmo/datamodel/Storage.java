/*
   Copyright (c) 2016 alehi
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mobilegeraete.kassel.uni.gizmo.datamodel;

import com.google.gson.annotations.Expose;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

public class Storage {


    //==========================================================================

    /********************************************************************
     * <pre>
     *              one                       one
     * Storage ----------------------------------- Dlw
     *              storage                   dlw
     * </pre>
     */

    public static final String PROPERTY_DLW = "dlw";
    /********************************************************************
     * <pre>
     *              one                       many
     * Storage ----------------------------------- Item
     *              storage                   items
     * </pre>
     */

    public static final String PROPERTY_ITEMS = "items";
    protected PropertyChangeSupport listeners = null;
    private Dlw dlw = null;
    @Expose
    private ArrayList<Item> items = null;

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }


    //==========================================================================

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    public void removeYou() {
        setDlw(null);
        withoutItems(this.getItems().toArray(new Item[this.getItems().size()]));
        firePropertyChange("REMOVE_YOU", this, null);
    }

    public Dlw getDlw() {
        return this.dlw;
    }

    public boolean setDlw(Dlw value) {
        boolean changed = false;

        if (this.dlw != value) {
            Dlw oldValue = this.dlw;

            if (this.dlw != null) {
                this.dlw = null;
                oldValue.setStorage(null);
            }

            this.dlw = value;

            if (value != null) {
                value.withStorage(this);
            }

            firePropertyChange(PROPERTY_DLW, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Storage withDlw(Dlw value) {
        setDlw(value);
        return this;
    }

    public Dlw createDlw() {
        Dlw value = new Dlw();
        withDlw(value);
        return value;
    }

    public ArrayList<Item> getItems() {
        if (this.items == null) {
            return null;
        }

        return this.items;
    }

    public Storage withItems(Item... value) {
        if (value == null) {
            return this;
        }
        for (Item item : value) {
            if (item != null) {
                if (this.items == null) {
                    this.items = new ArrayList<Item>();
                }

                boolean changed = false;
                if(!this.items.contains(item)){
                    changed = this.items.add(item);
                }
                if (changed) {
                    item.withStorage(this);
                    firePropertyChange(PROPERTY_ITEMS, null, item);
                }
            }
        }
        return this;
    }

    public Storage withoutItems(Item... value) {
        for (Item item : value) {
            if ((this.items != null) && (item != null)) {
                if (this.items.remove(item)) {
                    item.setStorage(null);
                    firePropertyChange(PROPERTY_ITEMS, item, null);
                }
            }
        }
        return this;
    }

    public Item findItem(String name){
        if(items != null){
            for(Item i : items){
                if(i.getName().equals(name)){
                    return i ;
                }
            }
        } return null;
    }

    public Item createItems() {
        Item value = new Item();
        withItems(value);
        return value;
    }

    public Food createItemsFood() {
        Food value = new Food();
        withItems(value);
        return value;
    }

    public Cleaning createItemsCleaning() {
        Cleaning value = new Cleaning();
        withItems(value);
        return value;
    }

    public Drinks createItemsDrinks() {
        Drinks value = new Drinks();
        withItems(value);
        return value;
    }

    public Clothing createItemsClothing() {
        Clothing value = new Clothing();
        withItems(value);
        return value;
    }

    public Plate createItemsPlate() {
        Plate value = new Plate();
        withItems(value);
        return value;
    }

    public Cup createItemsCup() {
        Cup value = new Cup();
        withItems(value);
        return value;
    }
}
