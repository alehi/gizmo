package com.mobilegeraete.kassel.uni.gizmo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class BootUpReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("BOOTUPRECEIVER called", "BOOTUPRECEIVER called");

        Intent serviceIntent = new Intent(context, BackgroundService.class);
        context.startService(serviceIntent);

    }
}