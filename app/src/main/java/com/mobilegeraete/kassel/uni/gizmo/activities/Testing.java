package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mobilegeraete.kassel.uni.gizmo.R;

public class Testing extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing);

        //Set up actionBar for Testing activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.dummy);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.testsTextView));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);
    }

    public void testingActions(View v) {
        Intent intent = new Intent(this, TestingNeeds.class);

        startActivity(intent);
    }

    public void testingGoods(View v) {
        Intent intent = new Intent(this, TestingItems.class);

        startActivity(intent);
    }
}
