package com.mobilegeraete.kassel.uni.gizmo.util;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.*;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alehi on 02.01.2017.
 */

public class Persistence {

    //the filename of our dataset
    private static String DEFAULT_NAME = "gizmo_data_set";

    private static Dlw dlw;

    public static Dlw getDlw() {
        return dlw;
    }


    public static void setDlw(Context context, Dlw dlw) {
        Persistence.dlw = dlw;
        dumpData(context);
        registerListener(context);
    }

    /**
     * Registers a listener to every property.
     * Dumps the data to internal storage whenever a property-change-event occurs
     * @param context The Application context, necessary for data dump
     */

    private static void registerListener(final Context context) {

        dlw.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                dumpData(context);

                if(propertyChangeEvent.getPropertyName().equals("needs")){
                    if(propertyChangeEvent.getNewValue() != null){
                        Needs need = (Needs) propertyChangeEvent.getNewValue();
                        need.addPropertyChangeListener(new PropertyChangeListener() {
                            @Override
                            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                                dumpData(context);
                            }
                        });
                    }
                }

                if(propertyChangeEvent.getPropertyName().equals("nextNeeds")){
                    if(propertyChangeEvent.getNewValue() != null){
                        Needs need = (Needs) propertyChangeEvent.getNewValue();
                        need.addPropertyChangeListener(new PropertyChangeListener() {
                            @Override
                            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                                dumpData(context);
                            }
                        });
                    }
                }
            }
        });

        if(dlw.getNeeds() != null){
            for (Needs need: dlw.getNeeds()) {
                if(need !=null) {
                    need.addPropertyChangeListener(new PropertyChangeListener() {
                        @Override
                        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                            dumpData(context);
                        }
                    });
                }
            }
            for (Needs need: dlw.getNextNeeds()) {
                if(need !=null) {
                    need.addPropertyChangeListener(new PropertyChangeListener() {
                        @Override
                        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                            dumpData(context);
                        }
                    });
                }
            }
        }

        //storage

        dlw.getStorage().addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                dumpData(context);
                if(propertyChangeEvent.getPropertyName().equals("items")){
                    if(propertyChangeEvent.getNewValue() != null){
                        Item item = (Item) propertyChangeEvent.getNewValue();
                        item.addPropertyChangeListener(new PropertyChangeListener() {
                            @Override
                            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                                dumpData(context);
                            }
                        });
                    }
                }
            }
        });

        if(dlw.getStorage().getItems() != null){
            for (Item i: dlw.getStorage().getItems()) {
                if(i !=null) {
                    i.addPropertyChangeListener(new PropertyChangeListener() {
                        @Override
                        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                            dumpData(context);
                        }
                    });
                }
            }
        }
    }



    /**
     * Dumps the data to the internal storage
     * @param context necessary for openFileOutput
     * @return true if successful
     */
    public static boolean dumpData(Context context){
        Log.d("Persistence","Persistence: dumping data...");

        String dataString = dlwToString();
        dataString = dataString.concat("\r\n$\r\n");
        dataString = dataString.concat(needsToString());
        dataString = dataString.concat("\r\n$\r\n");
        dataString = dataString.concat(nextNeedsToString());
        dataString = dataString.concat("\r\n$\r\n");
        dataString = dataString.concat(deliveryToString());
        dataString = dataString.concat("\r\n$\r\n");
        dataString = dataString.concat(itemsToString());


        try {
            FileOutputStream fos = context.openFileOutput(DEFAULT_NAME, Context.MODE_PRIVATE);
            fos.write(dataString.getBytes());
            Log.d("Persistence","Persistence: Datenbestand gesichert!");
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * Loads data from the internal storage
     * @param context necessary for openFileInput
     * @return true if successful
     */
    public static boolean loadDlw(Context context){

        String dataString = null;

        try {
            FileInputStream fis = context.openFileInput(DEFAULT_NAME);
            byte[] readBytes = new byte[fis.available()];
            fis.read(readBytes);
            dataString = new String(readBytes);
            Log.d("Persistence","Persistence: "+ dataString + " gelesen");

        } catch (FileNotFoundException e) {
            Log.d("Persistence","Persistence: Datei nicht gefunden.");
            return false;
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (dataString != null) {

            dataString = dataString.replace("\r", "");
            dataString = dataString.replace("\n", "");

            String tempString = dataString.substring(0, dataString.indexOf('$'));
            Dlw dlw = stringToDlw(tempString);
            dataString = dataString.substring(dataString.indexOf('$') + 1);

            tempString = dataString.substring(0, dataString.indexOf('$'));
            ArrayList<Needs> needs = stringToNeeds(tempString);
            dataString = dataString.substring(dataString.indexOf('$') + 1);

            tempString = dataString.substring(0, dataString.indexOf('$'));
            ArrayList<Needs> nextNeeds = stringToNextNeeds(tempString);
            dataString = dataString.substring(dataString.indexOf('$') + 1);

            tempString = dataString.substring(0, dataString.indexOf('$'));
            Delivery delivery = stringToDelivery(tempString);
            dataString = dataString.substring(dataString.indexOf('$') + 1);

            ArrayList<Item> items = stringToItems(dataString);

            //connect data

            for (int i = 0; i < needs.size(); i++) {
                dlw.withNeeds(needs.get(i));
            }

            for (int i = 0; i < nextNeeds.size(); i++) {
                dlw.withNextNeeds(nextNeeds.get(i));
            }

            dlw.withDelivery(delivery);

            Storage storage = new Storage();

            dlw.withStorage(storage);

            for (int i = 0; i < items.size(); i++) {
                storage.withItems(items.get(i));
            }
            setDlw(context, dlw);
            return true;
        }
        return false;
    }


    /**
     * Erases the dlw data-set
     * @param context necessary for deleteFile operation
     */
    public static void eraseData(Context context){
        String[] files = context.fileList();
        for(String s: files){
            Log.d("Persistence","Persistence "+ s +" gefunden");
            if(s.equals(DEFAULT_NAME)){
                Log.d("Persistence","Persistence "+ s +" geloescht");
                context.deleteFile(DEFAULT_NAME);
            }
        }
        dlw = null;
    }


    // String -> Items
    private static ArrayList<Item> stringToItems(String dataString) {


        ArrayList<Item> items = new ArrayList<Item>();
        Gson gson = new Gson();

        while(dataString.contains("?")){

            String className = dataString.substring(0, dataString.indexOf('?'));
            dataString = dataString.substring(dataString.indexOf('?')+1);
            String jsonString = dataString.substring(0,dataString.indexOf('?'));
            dataString = dataString.substring(dataString.indexOf('?')+1);

            if(Cleaning.class.getName().equals(className)){
                items.add(gson.fromJson(jsonString, Cleaning.class));
            } else if(Clothing.class.getName().equals(className)){
                items.add(gson.fromJson(jsonString, Clothing.class));
            } else if(Cup.class.getName().equals(className)){
                items.add(gson.fromJson(jsonString, Cup.class));
            } else if(Drinks.class.getName().equals(className)){
                items.add(gson.fromJson(jsonString, Drinks.class));
            } else if(Food.class.getName().equals(className)){
                items.add(gson.fromJson(jsonString, Food.class));
            } else if(Plate.class.getName().equals(className)){
                items.add(gson.fromJson(jsonString, Plate.class));
            }
        }

        return items;

    }

    // String -> Needs
    private static ArrayList<Needs> stringToNeeds(String dataString) {

        ArrayList<Needs> needs = new ArrayList<Needs>();
        Gson gson = new Gson();

        while(dataString.contains("%")){

            String className = dataString.substring(0, dataString.indexOf('%'));
            dataString = dataString.substring(dataString.indexOf('%')+1);
            String jsonString = dataString.substring(0,dataString.indexOf('%'));
            dataString = dataString.substring(dataString.indexOf('%')+1);

            if(Hunger.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Hunger.class));
            } else if(Play.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Play.class));
            } else if(Thirst.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Thirst.class));
            } else if(Dress.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Dress.class));
            } else if(Walk.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Walk.class));
            }

        }

        return needs;
    }

    // String -> NextNeeds
    private static ArrayList<Needs> stringToNextNeeds(String dataString) {

        ArrayList<Needs> needs = new ArrayList<Needs>();
        Gson gson = new Gson();

        while(dataString.contains("#")){

            String className = dataString.substring(0, dataString.indexOf('#'));
            dataString = dataString.substring(dataString.indexOf('#')+1);
            String jsonString = dataString.substring(0,dataString.indexOf('#'));
            dataString = dataString.substring(dataString.indexOf('#')+1);

            if(Hunger.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Hunger.class));
            } else if(Play.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Play.class));
            } else if(Thirst.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Thirst.class));
            } else if(Dress.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Dress.class));
            } else if(Walk.class.getName().equals(className)){
                needs.add(gson.fromJson(jsonString, Walk.class));
            }

        }
        return needs;
    }

    // String -> Delivery
    private static Delivery stringToDelivery(String dataString) {
        Gson gson = new Gson();
        return gson.fromJson(dataString, Delivery.class);
    }

    // String -> Dlw
    private static Dlw stringToDlw(String dataString) {
        Gson gson = new Gson();
        return gson.fromJson(dataString, Dlw.class);
    }

    // Delivery -> String
    private static String deliveryToString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
        String dataString = new String();

        dataString=gson.toJson(dlw.getDelivery());

        return dataString;
    }

    // Items -> String
    private static String itemsToString() {

        Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
        String dataString = new String();

        ArrayList<Item> items = dlw.getStorage().getItems();

        if(items != null){
            for(int i =0; i<items.size(); i++){

                String name = items.get(i).getClass().getName();
                dataString=dataString.concat(name + "\r\n?\r\n");
                dataString=dataString.concat(gson.toJson(items.get(i)));
                dataString=dataString.concat("\r\n?\r\n");
            }
        }

        return dataString;

    }

    // Needs -> String
    private static String needsToString() {

        Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
        String dataString = new String();
        List<Needs> needs = dlw.getNeeds();

        if(needs != null){
            for(int i =0; i<needs.size(); i++){

                String name = needs.get(i).getClass().getName();
                dataString=dataString.concat(name + "\r\n%\r\n");
                dataString=dataString.concat(gson.toJson(needs.get(i)));
                dataString=dataString.concat("\r\n%\r\n");
            }
        }
        return dataString;
    }

    // NextNeeds -> String
    private static String nextNeedsToString() {

        Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
        String dataString = new String();
        List<Needs> needs = dlw.getNextNeeds();

        if(needs != null){
            for(int i =0; i<needs.size(); i++){

                String name = needs.get(i).getClass().getName();
                dataString=dataString.concat(name + "\r\n#\r\n");
                dataString=dataString.concat(gson.toJson(needs.get(i)));
                dataString=dataString.concat("\r\n#\r\n");
            }
        }
        return dataString;
    }

    // Dlw -> String
    private static String dlwToString() {

        Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();

        return gson.toJson(dlw);
    }



}
