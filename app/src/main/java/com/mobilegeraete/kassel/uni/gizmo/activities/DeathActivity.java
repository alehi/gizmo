package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DeathActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_death);

        //Hide actionBar for Death activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        Dlw dlw = Persistence.getDlw();

        //Set TextViews

        TextView name = (TextView) findViewById(R.id.deathNameTV);
        name.setText(dlw.getName());

        TextView birthday = (TextView) findViewById(R.id.deathBirthdayTV);
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date birthdayDate = new Date(dlw.getCreated());
        birthday.setText(sdf.format(birthdayDate));

        TextView deathDay = (TextView) findViewById(R.id.deathDeathdateTV);
        Date deathDate = new Date(dlw.getDeathTime());
        deathDay.setText(sdf.format(deathDate));

        TextView age = (TextView) findViewById(R.id.deathAgeTV);
        Long timeLived = System.currentTimeMillis() - dlw.getCreated();
        age.setText(this.getString(R.string.deathDeathMsgPart1) + " " + (timeLived / (1000 * 60 * 60 * 24)) + " " + this.getString(R.string.deathDeathMsgPart2));

        // Set Variables to start Values
        Variables.remainingTime = Constants.SHOPPING_DURATION;
        Variables.buyProcessRunning = false;
        Variables.countDownTimerActive = false;
        Variables.backToMainMenu = false;
        Variables.buyButtonTimerActive = false;
        Variables.countSelectedItems = 1;
        Variables.selections.clear();
        Persistence.getDlw().setDelivery(null);
    }

    public void okButtonAction(View v) {

        Persistence.eraseData(this);


        Intent intent = new Intent(this, StartActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        return;
    }
}
