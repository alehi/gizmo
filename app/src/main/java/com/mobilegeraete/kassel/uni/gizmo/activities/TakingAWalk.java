package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Needs;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Walk;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

public class TakingAWalk extends AppCompatActivity implements SensorEventListener {

    //TextView
    private TextView takingAWalkDescriptionTV, takingAWalkDoneTV;

    //Progressbar
    private ProgressBar progressbar;

    // Dlw
    private Dlw dlw = Persistence.getDlw();

    //Sensor
    private SensorManager sM;
    private Sensor stepCounterSensor;

    private boolean activityRunning;

    private boolean startCounting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taking_awalk);

        //Set up actionBar for TakingAWalk activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.stroll);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.takingAWalk));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);

        sM = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        stepCounterSensor = sM.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        //Get the textView ids to access them later
        takingAWalkDescriptionTV = (TextView) findViewById(R.id.takingAWalkDescriptionTV);
        takingAWalkDoneTV = (TextView) findViewById(R.id.takingAWalkDoneTV);

        // Description for user
        takingAWalkDescriptionTV.setText(dlw.getName() + " " + getString(R.string.takingAWalkDescriptionTV));
        // State of the activity
        takingAWalkDoneTV.setText(dlw.getName() + " " + getString(R.string.takingAWalkStartTV));

        //Set the stepCount variable to 0
        //Variables.stepCount = 0;

        //Get progessbar to access them later
        progressbar = (ProgressBar) findViewById(R.id.takingAWalkProgressBar);
        /*
        progressbar.setMax(Constants.STEP_LIMIT);
        progressbar.setProgress(Variables.stepCount);
        */

        //Check, if the users device features the step_counter that is needed to count the steps.
        PackageManager pm = getPackageManager();
        if (!pm.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER)) {
            Toast.makeText(this, getString(R.string.takingAWalkNoCounterToast), Toast.LENGTH_LONG).show();
        }

        /*
        if(!pm.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_DETECTOR)){
            Toast.makeText(this, getString(R.string.takingAWalkNoStepDetectorToast), Toast.LENGTH_LONG).show();
        }
        */
        Drawable progressBarDrawable0 = getResources().getDrawable(R.drawable.progress_pawprints0);
        progressbar.setProgressDrawable(progressBarDrawable0);

        //To get the initial stepCount from the sensor the value of this variable is checked.
        // If true, the sensorEvent value is stored in Variables.startStepCountValue.
        startCounting = true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Set the activityRunning variable to false. The variable is needed to determent, if the activity is opened or not
        activityRunning = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Set the activityRunning variable to true. The activity is running.
        activityRunning = true;

        //Register the stepCounterSensor
        if (stepCounterSensor != null) {
            sM.registerListener(this, stepCounterSensor, SensorManager.SENSOR_DELAY_FASTEST);
        } else {
            Toast.makeText(this, getString(R.string.takingAWalkNoSensorToast), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();


        //Variables.stepCount = 0;

        //Set the startStepCountValue Variable to 0 and startCounting to true.
        //The user has to start at the beginning after closing the app.
        startCounting = true;
        Variables.startStepCountValue = 0;

        //unregister the listener
        sM.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        //Toast.makeText(this,"onSensorChanged", Toast.LENGTH_LONG).show();

        //If the activityRunning variable is true which means that the activity is running in the foreground.
        //The stepCount variable is increased for each step the user took.
        if (activityRunning) {
            //Variables.stepCount++;
            //takingAWalkDoneTV.setText(dlw.getName() + " " + getString(R.string.takingAWalkActiveTV));

            if (startCounting) {
                Variables.startStepCountValue = (int) sensorEvent.values[0];
                startCounting = false;
            }
        }

        Variables.diffStepCountValue = (int) sensorEvent.values[0] - Variables.startStepCountValue;

        //progressbar.setProgress(Variables.stepCount);

        // Progress Drawables
        Drawable progressBarDrawable1 = getResources().getDrawable(R.drawable.progress_pawprints1);
        Drawable progressBarDrawable2 = getResources().getDrawable(R.drawable.progress_pawprints2);
        Drawable progressBarDrawable3 = getResources().getDrawable(R.drawable.progress_pawprints3);
        Drawable progressBarDrawable4 = getResources().getDrawable(R.drawable.progress_pawprints4);
/*
        if(Variables.stepCount >= (Constants.STEP_LIMIT*0.25)){
            progressbar.setProgressDrawable(progressBarDrawable1);
        }
        if(Variables.stepCount > (Constants.STEP_LIMIT*0.50)){
            progressbar.setProgressDrawable(progressBarDrawable2);
        }
        if(Variables.stepCount > (Constants.STEP_LIMIT*0.75)){
            progressbar.setProgressDrawable(progressBarDrawable3);
        }
        */

        if (Variables.diffStepCountValue >= (Constants.STEP_LIMIT * 0.25)) {
            progressbar.setProgressDrawable(progressBarDrawable1);
        }
        if (Variables.diffStepCountValue > (Constants.STEP_LIMIT * 0.50)) {
            progressbar.setProgressDrawable(progressBarDrawable2);
        }
        if (Variables.diffStepCountValue > (Constants.STEP_LIMIT * 0.75)) {
            progressbar.setProgressDrawable(progressBarDrawable3);
        }

        // Walking finished
        //if(Variables.stepCount >= Constants.STEP_LIMIT ){
        if (Variables.diffStepCountValue >= Constants.STEP_LIMIT) {
            //Toast.makeText(this, getString(R.string.walkingDoneToast), Toast.LENGTH_LONG).show();
            //dlw = Persistence.getDlw();
            for (Needs need : dlw.getNeeds()) {
                if (need instanceof Walk) {
                    dlw.withoutNeeds(need);
                }
            }
            // Progressbar finished
            progressbar.setProgressDrawable(progressBarDrawable4);
            // TextView info for user
            takingAWalkDoneTV.setText(getString(R.string.takingAWalkDone1TV) + " " + dlw.getName() + " " + getString(R.string.takingAWalkDone2TV));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    //Method creates an intent and starts the mainMenu activity
    public void goToMainMenu(View v) {
        Intent intent = new Intent(this, MainMenu.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;
    }
}
