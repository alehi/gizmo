package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Walk;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //Set up actionBar for Settings activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.settings);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.settingsTitle));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);

        Button settingsSuicideBtn = (Button) findViewById(R.id.settingsSuicideBtn);

        //When the user accesses the settings activity from the startActivity the suicide button in the settings activity
        //has to be deactivated to prevent a crash
        if (Persistence.getDlw() == null) {
            settingsSuicideBtn.setVisibility(View.GONE);
        }
    }

    /**
     * Pushes a exceeded Need into the NeedsList and goes back to the MainMenu,
     * which will find the killerNeed and calls the DeathActivity.
     *
     * @param v The View (not the Talkshow)
     */
    public void suicide(View v) {

        Walk killerNeed = new Walk();

        //Sets the deadline to currentTimeMillis
        killerNeed.withStartTime(System.currentTimeMillis() - Constants.SATISFY_WALK);

        //push the need into the needsList
        Persistence.getDlw().withNeeds(killerNeed);

        //go back to MainMenu
        Intent intent = new Intent(this, MainMenu.class);
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
